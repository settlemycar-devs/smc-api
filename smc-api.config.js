module.exports = {
    apps: [
        {
            name: 'smc-api',
            script: 'server.js',
            env: {
                'NODE_ENV': 'development',
                'watch': ['app.js', 'bin', 'classes', 'config', 'constants', 'controllers', 'db_models', 'es', 'feed', 'middlewares', 'modules', 'package.json', 'routes', 'services', 'views'],
                'ignore_watch': ['public', 'node_modules', '.git'],
                'args': ['--ignore-watch=public|\.git|node_modules'],
                'node_args': ['--inspect=7001 --trace-deprecation'],
            },
            env_production: {
                'NODE_ENV': 'production',
                'instances': 'max',
                'exec_mode': 'cluster',
                'watch': false,
                'node_args  ': [],
            },
            env_staging: {
                'NODE_ENV': 'staging',
                'instances': 'max',
                'exec_mode': 'cluster',
                'watch': false,
                'node_args  ': ['--inspect=7001 --trace-deprecation'],
            },
        }
    ],
};
