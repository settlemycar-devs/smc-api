const QuotationModal = require('../db_models/quotation');
const _ = require('lodash');
const config = require('config');
const userService = require('./users');
const appContants = require('../constants/app');
const quotationStatusConstant = require('../constants/schema').quotationStatus;
const Utils = require('../utils');
const sesService = require('./aws/ses');
const awsConstants = require('../constants/aws');

class Reviews {
    get model() {
        return QuotationModal;
    }

    standardSort() {
        return {
            'created_at': -1,
        };
    }

    async reviewQuote(user, data) {
        const response = { check_email: false, };
        if (!user.email) {
            response.check_email = true;
        }
        return response;
    }

    async addQuoteRequest(user, data) {
        // eslint-disable-next-line global-require
        const GarageService = require('./garages');

        const email = _.get(data, 'email');
        const garageId = _.get(data, 'garage_id');
        const existingRecord = await this.getByQuery({ 'requested_by.id': user._id, 'garage_id': garageId, 'status': { '$nin': [quotationStatusConstant.COMPLETED], }, });
        if (existingRecord) {
            throw new Error('You have already requested Quote from this garage');
        }
        const quotationPayload = {
            requested_by: {
                id: _.get(user, '_id'),
                name: _.get(user, 'profile.name', ''),
                avatar: _.get(user, 'profile.avatar', ''),
                mobile: _.get(user, 'phone', ''),
                email: _.get(user, 'email', ''),
            },
            garage_id: _.get(data, 'garage_id'),
            request_detail: {
                brand_name: _.get(data, 'brand_name', ''),
                model: _.get(data, 'model', ''),
                year: _.get(data, 'year', ''),
                other_details: _.get(data, 'other_details', ''),
                location: { type: 'Point', coordinates: _.get(data, 'location'), },
                address: _.get(data, 'address', ''),
                service_name: _.get(data, 'service_name', ''),
            },
        };
        try {
            const garageData = await GarageService.getGarageByQuery({}, { _id: garageId, });
            const garageDetail = {
                id: garageData._id,
                name: garageData.name,
                avatar: garageData.avatar,
                address: garageData.address,
                location: garageData.location,
                rating: garageData.overall_rating,
            };
            quotationPayload.garage_detail = garageDetail;
            if (email) {
                await userService.updateOne(user._id, { email, });
            }
            const quotation = new this.model(quotationPayload);
            const newQuotation = await quotation.save();
            const response = {
                success: true,
                request_id: newQuotation.request_id,
            };
            return response;
        } catch (error) {
            throw error;
        }
    }

    async getByQuery(query = {}) {
        try {
            const data = await this.model.findOne(query);
            return data;
        } catch (error) {
            throw error;
        }
    }

    async addQuoteRequestWithUserCheck(user, data) {
        try {
            const query = { 'requested_by.id': user._id, 'garage_id': _.get(data, 'garage_id'), };
            const quoteRequest = await this.getByQuery(query);
            if (quoteRequest) {
                throw new Error('User has already requested quote with this garage');
            }
            this.addQuoteRequest(user, data);
        } catch (error) {
            throw error;
        }
    }

    validateAndRefineQuery(query) {
        const newQuery = {};
        if (query.filter) {
            for (const key of Object.keys(query.filter)) {
                _.assign(newQuery, { [key]: query.filter[key], });
            }
            delete query.filter;
        }
        _.assign(newQuery, query);
        return newQuery;
    }

    async getAll(user = {}, query = {}, options = {}) {
        const validatedQuery = this.validateAndRefineQuery(query);
        const limit = options.limit || appContants.STANDARD_LIST_LIMIT;
        const skip = options.skip || appContants.DEFAULT_SKIP;
        const sort = options.sort || this.standardSort();

        const findPromise = this.model.find(validatedQuery).skip(skip).sort(sort);
        if (limit !== appContants.STANDARD_LIST_NO_LIMIT) {
            findPromise.limit(limit);
        }

        try {
            const quoteRequests = await findPromise;
            return {
                data: quoteRequests,
                meta: {
                    limit,
                    offset: skip + quoteRequests.length,
                    has_next: quoteRequests.length !== 0,
                },
            };
        } catch (error) {
            throw error;
        }
    }

    async getAllQuoteRequestByUser(userId, query = {}) {
        const newQuery = { 'requested_by.id': userId, ...query, };
        return this.getAll({}, newQuery, { limit: appContants.STANDARD_LIST_NO_LIMIT, });
    }

    async updateOne(id, updates) {
        try {
            const response = await this.model.updateOne({ _id: id, }, updates);
            return response;
        } catch (error) {
            throw error;
        }
    }

    async sendRequestToGarageViaMail(quoteRequest) {
        // eslint-disable-next-line global-require
        const garageService = require('./garages');
        const sesQuoteSender = config.get('aws.SES_QUOTE_SENDER');
        try {
            const garageDetail = await garageService.getOne({ _id: quoteRequest.garage_id, }, { select: 'owner_info', });
            if (garageDetail && garageDetail.owner_info && garageDetail.owner_info.email) {
                const body = awsConstants.emailTemplates.requestQuoteEmailBody();
                const subject = awsConstants.emailTemplates.requestQuoteEmailSubject();
                const result = await sesService.sendMail({
                    sender: sesQuoteSender,
                    recipient: garageDetail.owner_info.email,
                    body,
                    subject,
                });

                // update to db that request to garage
                const communication = quoteRequest.communication || {};
                _.assign(communication, { request_sent_to_garage_via_mail: true, });
                const updateResponse = await this.updateOne(quoteRequest._id, {});
                console.log('Mail sent Response:', result);
                console.log('Update REsponse', updateResponse);
            }
        } catch (error) {
            console.log('error sending quote request to garage', error);
        }
    }

    async sendQuoteToUserViaMail(quoteRequest) {
        const sesQuoteSender = config.get('aws.SES_QUOTE_SENDER');
        try {
            const userDetail = await userService.getOne({ _id: quoteRequest.requested_by.id, }, { select: 'email', });
            if (userDetail && userDetail.email) {
                const body = awsConstants.emailTemplates.sentQuoteEmailBody(quoteRequest);
                const subject = awsConstants.emailTemplates.sentQuoteEmailSubject(quoteRequest);
                const result = await sesService.sendMail({
                    sender: sesQuoteSender,
                    recipient: userDetail.email,
                    body,
                    subject,
                });

                // update to db that request to garage
                const communication = quoteRequest.communication || {};
                _.assign(communication, { quote_sent_to_user_via_mail: true, });
                const updateResponse = await this.updateOne(quoteRequest._id, { communication, });
                console.log('Mail sent Response:', result);
                console.log('Update Response', updateResponse);
            }
        } catch (error) {
            console.log('error sending quote request to garage', error);
        }
    }

    async updateQuoteRequest(user, requestId, update = {}) {
        try {
            let notifyRequestToGarage = false;
            let notifyQuoteToUser = false;
            let query = { _id: requestId, };
            if (!Utils.isObjectId(requestId)) {
                query = { request_id: requestId, };
            }
            const quoteRequest = await this.model.findOne(query);
            if (update.status) {
                quoteRequest.status = update.status;
                if (update.status === quotationStatusConstant.REQUEST_SENT && update.communicate) {
                    notifyRequestToGarage = true;
                }
                if (update.status === quotationStatusConstant.QUOTE_SENT && update.communicate) {
                    notifyQuoteToUser = true;
                }
            }
            if (update.quote_detail) {
                let updatedQuoteDetail = quoteRequest.quote_detail || {};
                updatedQuoteDetail = _.assign(updatedQuoteDetail, update.quote_detail);
                quoteRequest.quote_detail = updatedQuoteDetail;
            }
            const updatedRequest = await quoteRequest.save();
            if (notifyRequestToGarage) {
                await this.sendRequestToGarageViaMail(updatedRequest);
            }
            if (notifyQuoteToUser) {
                await this.sendQuoteToUserViaMail(updatedRequest);
            }
            return updatedRequest;
        } catch (error) {
            throw error;
        }
    }

    async acceptQuoteRequest(user, requestId, update = {}) {
        try {
            let query = { _id: requestId, };
            if (!Utils.isObjectId(requestId)) {
                query = { request_id: requestId, };
            }
            const quoteRequest = await this.model.findOne(query);
            quoteRequest.status = quotationStatusConstant.COMPLETED;
            // TODO: Notify garages that quote has accepted by the user, call user
            const updatedRequest = await quoteRequest.save();
            return updatedRequest;
        } catch (error) {
            throw error;
        }
    }

    async getAllQuoteRequest(user = {}, query = {}, options = {}) {
        const validatedQuery = this.validateAndRefineQuery(query);
        const limit = options.limit || appContants.STANDARD_LIST_LIMIT;
        const skip = options.skip || appContants.DEFAULT_SKIP;
        const sort = options.sort || this.standardSort();

        const findPromise = this.model.find(validatedQuery).skip(skip).sort(sort);
        if (limit !== appContants.STANDARD_LIST_NO_LIMIT) {
            findPromise.limit(limit);
        }

        try {
            const quoteRequests = await findPromise;
            return {
                data: quoteRequests,
                meta: {
                    limit,
                    offset: skip + quoteRequests.length,
                    has_next: quoteRequests.length !== 0,
                },
            };
        } catch (error) {
            throw error;
        }
    }
}

module.exports = new Reviews();
