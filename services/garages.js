const _ = require('lodash');
const GarageModel = require('../db_models/garages');
const quotationService = require('./quotation');
const appContants = require('../constants/app');
const quotationStatus = require('../constants/schema').quotationStatus;
const Utils = require('../utils');
const config = require('config');

class Garages {
    standardSort() {
        return {
            'created_at': -1,
        };
    }

    async addGarage(user, data) {
        // TODO: Handle garage creation at the time insertion, get mobile no. and create user if doesn't exist
        try {
            if (user) {
                data.created_by = {
                    id: user.id,
                    name: user.profile.name,
                    avatar: user.profile.avatar,
                };
            }
            const newGarage = new GarageModel(data);
            return await newGarage.save();
        } catch (error) {
            throw error;
        }
    }

    // FIXME: response is not returning correct data
    async addManyGarages(data) {
        const results = [];
        _.each(data, async garageData => {
            try {
                const newGarage = new GarageModel(garageData);
                const result = await newGarage.save();
                console.log('result is', result);
                results.push(result);
            } catch (error) {
                results.push(error);
            }
        });
        console.log('return result', results);
        return results;
    }

    async addMechanics(id, data) {
        try {
            const garageData = await GarageModel.findOne({ _id: id, });
            if (!garageData) {
                throw new Error('No garage found');
            }
            const mechanics = garageData.mechanics || [];
            const newMechanics = mechanics.concat(data.mechanics);
            garageData.mechanics = newMechanics;
            return await garageData.save();
        } catch (error) {
            throw error;
        }
    }

    addHasQuoteRequest(quoteRequests, garages) {
        const requestGaragesIds = _.map(quoteRequests, quote => quote.garage_id.toString());
        const garageData = _.map(garages, garage => {
            if (requestGaragesIds.includes(garage._id.toString())) {
                garage.has_quote_request = true;
                const quoteRequestDataIndex = _.findIndex(quoteRequests, item => item.garage_id.toString() === garage._id.toString());
                garage.requested_status = quoteRequests[quoteRequestDataIndex].status;
            }
            return garage;
        });
        return garageData;
    }

    async getGarageByQuery(user, query = {}) {
        try {
            const garageData = await GarageModel.findOne(query).lean();
            if (user) {
                const quoteRequest = await quotationService.getAllQuoteRequestByUser(user._id, { status: { $in: [quotationStatus.CREATED, quotationStatus.REQUEST_SENT, quotationStatus.QUOTE_SENT], }, });
                const garageDataWithQuoteRequest = this.addHasQuoteRequest(quoteRequest.data, [garageData])[0];
                return garageDataWithQuoteRequest;
            }
            return garageData;
        } catch (error) {
            throw error;
        }
    }

    async getOne(query, options = {}) {
        try {
            const garageDataPromise = GarageModel.findOne(query);
            if (options.select) {
                garageDataPromise.select(options.select);
            }
            const garageData = await garageDataPromise.lean();
            return garageData;
        } catch (error) {
            throw error;
        }
    }

    async getAllGarages(user, query = {}, options = {}) {
        const limit = options.limit || appContants.STANDARD_LIST_LIMIT;
        const offset = options.skip || options.offset || appContants.DEFAULT_SKIP;
        const sort = options.sort || this.standardSort();
        try {
            if (query.latitude && query.longitude) {
                const lat = query.latitude;
                const lng = query.longitude;
                delete query.latitude;
                delete query.longitude;
                query.location = {
                    $near: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [lng, lat],
                        },
                        $maxDistance: config.get('app.MAX_DISTANCE_TO_SEARCH_GARAGES'),
                    },
                };
            }
            if (!Utils.isProd()) {
                query = {};
            }
            const garages = await GarageModel.find(query).skip(offset).limit(limit).sort(sort)
                .lean();
            if (user) {
                const quoteRequest = await quotationService.getAllQuoteRequestByUser(user._id, { status: { $in: [quotationStatus.CREATED, quotationStatus.REQUEST_SENT, quotationStatus.QUOTE_SENT], }, });
                const garageData = this.addHasQuoteRequest(quoteRequest.data, garages);
                garages.data = garageData;
            }
            return {
                data: garages,
                meta: {
                    limit,
                    offset: offset + garages.length,
                    has_next: garages.length !== 0,
                },
            };
        } catch (error) {
            throw error;
        }
    }

    async updateGarage(user, garageId, updates = {}) {
        try {
            if (user) {
                updates.updated_by = {
                    id: user.id,
                    name: user.profile.name,
                    avatar: user.profile.avatar,
                };
            }
            const garage = await GarageModel.findOne({ _id: garageId, });
            _.assign(garage, updates);
            const updatedGarage = await garage.save();
            return updatedGarage;
        } catch (error) {
            throw error;
        }
    }

    filterGenericUpdates(data) {
        const filteredData = {};
        if (_.isEmpty(data)) {
            return filteredData;
        }
        data.faqs && _.assign(filteredData, { faqs: data.faqs, });
        data.name && _.assign(filteredData, { name: data.name, });
        data.description && _.assign(filteredData, { description: data.description, });
        data.address && _.assign(filteredData, { address: data.address, });
        data.pincode && _.assign(filteredData, { pincode: data.pincode, });
        data.location && data.location.coordinates && _.assign(filteredData, { location: data.location, });
        data.media && data.media.source && _.assign(filteredData, { media: data.media, });
        data.established_since && _.assign(filteredData, { established_since: data.established_since, });
        data.overall_rating && _.assign(filteredData, { overall_rating: data.overall_rating, });
        data.facilities && _.assign(filteredData, { facilities: data.facilities, });
        data.garage_in_numbers && _.assign(filteredData, { garage_in_numbers: data.garage_in_numbers, });
        data.tags && _.assign(filteredData, { tags: data.tags, });
        data.average_response_time && _.assign(filteredData, { average_response_time: data.average_response_time, });

        return filteredData;
    }

    async updateGeneric(user, garageId, updates = {}) {
        try {
            const dataToUpdate = this.filterGenericUpdates(updates);
            return this.updateGarage(user, garageId, dataToUpdate);
        } catch (error) {
            throw error;
        }
    }
}

module.exports = new Garages();
