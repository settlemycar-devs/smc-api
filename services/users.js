const _ = require('lodash');
const UserModel = require('../db_models/users');

class Users {
    get model() {
        return UserModel;
    }

    async getOne(query, options = {}) {
        try {
            const userPromise = this.model.findOne(query);
            if (options.select) {
                userPromise.select(options.select);
            }
            if (options.lean) {
                userPromise.lean();
            }
            const user = await userPromise;
            return user;
        } catch (error) {
            throw error;
        }
    }

    async addOne(data) {
        try {
            const user = await new UserModel(data).save();
            return user;
        } catch (error) {
            throw error;
        }
    }

    async updateOne(id, update = {}) {
        try {
            const updates = await this.model.updateOne({ _id: id, }, update);
            return updates;
        } catch (error) {
            throw error;
        }
    }

    getProfileFromUser(user) {
        const userObject = user.toObject();
        const result = _.omit(userObject, ['password', 'otp', 'method', '__v']);
        return result;
    }

    async updateUserProfile(userId, update = {}) {
        const user = await this.model.findOne({ _id: userId, });
        if (update.email) {
            user.email = update.email;
            delete update.email;
        }
        const profile = user.profile || {};
        const dataToBeUpdate = _.assign(profile, update);
        user.profile = dataToBeUpdate;
        const updatedUser = await user.save();
        const userProfile = this.getProfileFromUser(updatedUser);
        return userProfile;
    }
}

module.exports = new Users();
