const BrandModel = require('../db_models/brands');

class App {
    standardSort() {
        return {
            'created_at': -1,
        };
    }

    async getBrands() {
        try {
            const brands = await BrandModel.find({});
            return brands;
        } catch (error) {
            throw error;
        }
    }

    async handleBulkInsert(user, data) {
        try {
            const result = await BrandModel.insertMany(data);
            return result;
        } catch (error) {
            throw error;
        }
    }
}

module.exports = new App();
