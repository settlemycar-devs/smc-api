const _ = require('lodash');
const config = require('config');
const geoip = require('geoip-lite');

const request = require('request-promise');

class PlacesService {
    get googleApiKey() {
        return config.get('app.GOOGLE_API_KEY');
    }

    getGeoLocation(ip) {
        try {
            const geo = geoip.lookup(ip);
            console.log('geoData for ip', ip);
            console.log(JSON.stringify(geo, null, 2));
            const data = {
                latitude: geo.ll[0],
                longitude: geo.ll[1],
                city: geo.city,
                country: geo.country,
            };
            return data;
        } catch (error) {
            return {};
        }
    }

    getAutocompletePlaces(query, ipAddress) {
        let latitude = _.get(query, 'latitude');
        let longitude = _.get(query, 'longitude');
        if ((!latitude || !longitude) && ipAddress) {
            const geoData = this.getGeoLocation(ipAddress);
            latitude = _.get(geoData, 'latitude');
            longitude = _.get(geoData, 'longitude');
        }
        if (!latitude || !longitude) {
            latitude = 28.7041;
            longitude = 77.1025;
        }
        const inputQuery = _.get(query, 'query');
        const uri = `https://maps.googleapis.com/maps/api/place/autocomplete/json`;
        const result = [];
        const qs = {
            input: inputQuery,
            key: this.googleApiKey,
            location: `${latitude},${longitude}`,
            language: 'en',
            radius: 100000,
            components: 'country:in',
        };
        return request(uri, {
            method: 'GET',
            qs,
            json: true,
        }).then(response => {
            _.map(response.predictions, placeData => {
                result.push({
                    title: placeData.structured_formatting.main_text,
                    description: placeData.description,
                    place_id: placeData.place_id,
                    id: placeData.id,
                    location: {
                        latitude,
                        longitude,
                    },
                });
            });
            return result;
        }).catch(err => {
            console.log('error occured while fetching place', err);
            return result;
        });
    }

    getPlaceDetails(query) {
        const placeId = _.get(query, 'place_id');

        const uri = `https://maps.googleapis.com/maps/api/place/details/json`;

        return request(uri, {
            method: 'GET',
            useQuerystring: true,
            qs: {
                place_id: placeId,
                key: this.googleApiKey,
            },
            json: true,
        }).then(response => {
            const latitude = _.get(response.result, 'geometry.location.lat');
            const longitude = _.get(response.result, 'geometry.location.lng');
            return {
                latitude,
                longitude,
            };
        }).catch(err => {
            console.log('error occured while fetching place details', err);
            return {};
        });
    }
}

module.exports = new PlacesService();
