const ReviewsModel = require('../db_models/review');
const appContants = require('../constants/app');

class Reviews {
    standardSort() {
        return {
            'createdAt': -1,
        };
    }

    async addReview(data) {
        try {
            const newReview = new ReviewsModel(data);
            return await newReview.save();
        } catch (error) {
            throw error;
        }
    }

    async getAllReviews(query = {}, options = {}) {
        const limit = options.limit || appContants.STANDARD_LIST_LIMIT;
        const offset = options.skip || options.offset || appContants.DEFAULT_SKIP;
        const sort = options.sort || this.standardSort();
        try {
            const reviews = await ReviewsModel.find(query).skip(offset).limit(limit).sort(sort);
            return {
                data: reviews,
                meta: {
                    limit,
                    offset: offset + reviews.length,
                    has_next: reviews.length !== 0,
                },
            };
        } catch (error) {
            throw error;
        }
    }
}

module.exports = new Reviews();
