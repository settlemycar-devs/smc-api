const AWS = require('aws-sdk');
const config = require('config');

const accesKey = config.get('aws.AWS_ACCESS_KEY');
const secretKey = config.get('aws.AWS_SECRET_KEY');
const region = config.get('aws.AWS_SES_REGION');
const isEmailEnabled = config.get('aws.IS_EMAIL_ENABLED');

AWS.config.update({
    accessKeyId: accesKey,
    secretAccessKey: secretKey,
    region,
    apiVersions: {
        ses: '2010-12-01',
    },
});

class SES {
    verifyEmail(emails) {
        const isValidEmail = true;
        // for (const email of emails) {

        // }
        return isValidEmail;
    }

    async sendMail({ sender, recipient, subject, body, cc, bcc, }) {
        if (!isEmailEnabled) {
            return;
        }

        const recipients = recipient ? [recipient] : [];
        const isValidEmail = this.verifyEmail(recipients);
        if (!isValidEmail) {
            console.log('Invalid Email aborting request:', recipients);
            return;
        }
        const ccEmails = cc || [];
        const bccEmails = bcc || [];
        const params = {
            Destination: {
                BccAddresses: bccEmails,
                CcAddresses: ccEmails,
                ToAddresses: recipients,
            },
            Message: {
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: body,
                    },
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: subject,
                },
            },
            ReplyToAddresses: [], // when use reply that email will to these email addresses
            Source: `Garage Troop <${sender}>`,
            ReturnPath: sender, // bounce email error message will be sent to this email
        };
        const ses = new AWS.SES({ region, });

        try {
            const response = await ses.sendEmail(params).promise();
            console.log('Email Send:', response);
            return response;
        } catch (error) {
            console.log('error', error);
            throw error;
        }
    }

    sendBulkmail({ sender, recipients, subject, body, cc, bcc, }) {

    }
}

module.exports = new SES();
