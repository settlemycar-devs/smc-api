// by default load .env file in root directory
// can be loaded at run time
// node -r dotenv/config server.js dotenv_config_path=/custom/path/to/envs
// dotenv can't override already declared environment variables
require('dotenv').config();

const config = {
    app: {
        PORT: process.env.PORT,
        JWT_SECRET: process.env.JWT_SECRET,
        SECRET_KEY: process.env.SECRET_KEY,
        GOOGLE_API_KEY: process.env.GOOGLE_API_KEY,
        MAX_DISTANCE_TO_SEARCH_GARAGES: Number(process.env.MAX_DISTANCE_TO_SEARCH_GARAGES) || 10000, // default is 10 KM
    },
    db: {
        DB_URI: process.env.DB_URI,
        DEBUG: true,
    },
    sms: {
        SMS_API_KEY: process.env.SMS_API_KEY,
    },
    aws: {
        AWS_ACCESS_KEY: process.env.AWS_ACCESS_KEY,
        AWS_SECRET_KEY: process.env.AWS_SECRET_KEY,
        AWS_SMS_REGION: process.env.AWS_SMS_REGION,
        AWS_REGION: process.env.AWS_REGION,
        AWS_BUCKET: process.env.AWS_BUCKET,
        GARAGE_PROFILE_FOLDER: process.env.GARAGE_PROFILE_FOLDER,
        GARAGE_GALLARY_FOLDER: process.env.GARAGE_GALLARY_FOLDER,
        AWS_SES_REGION: process.env.AWS_SES_REGION,
        SES_QUOTE_SENDER: process.env.SES_QUOTE_SENDER,
        IS_EMAIL_ENABLED: Number(process.env.IS_EMAIL_ENABLED),
    },
};

module.exports = config;
