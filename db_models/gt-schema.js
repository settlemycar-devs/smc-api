const mongoose = require('mongoose');
const _ = require('lodash');
const Utils = require('../utils');

const SchemaTypes = Utils.schemaTypes();

const ACTOR_SCHEMA = {
    id: SchemaTypes.MANDATORY_OBJECT_ID,
    name: SchemaTypes.OPTIONAL_STRING,
    avatar: SchemaTypes.OPTIONAL_STRING,
};

const { Schema, } = mongoose;
class GTSchema extends Schema {
    constructor(schemaData) {
        const metaSchema = {
            created_by: { type: ACTOR_SCHEMA, required: false, },
            updated_by: { type: ACTOR_SCHEMA, required: false, },
            deleted_by: { type: ACTOR_SCHEMA, required: false, },
            is_deleted: SchemaTypes.MANDATORY_BOOLEAN,
            deleted_at: SchemaTypes.OPTIONAL_DATE,
        };
        const schema = _.assign({}, metaSchema, schemaData);
        super(schema, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at', }, });

        // this hooks called first, then other declared hooks called at schema level.
        this.pre('find', function () {
            const query = this.getQuery();
            if (!('is_deleted' in query)) {
                this.where('$or', [{ 'is_deleted': { '$exists': false, }, }, { is_deleted: false, }]);
            }
        });
    }
}

module.exports = GTSchema;
