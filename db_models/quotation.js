const conn = require('../modules/db');
const shortId = require('shortid');
const _ = require('lodash');

const GTSchema = require('./gt-schema');
const SchemaTypes = require('../utils').schemaTypes();
const { quotationStatus, } = require('../constants/schema');

const REQUEST_DETAIL_SCHEMA = {
    brand_name: SchemaTypes.OPTIONAL_STRING,
    model: SchemaTypes.OPTIONAL_STRING,
    year: SchemaTypes.OPTIONAL_STRING,
    other_details: SchemaTypes.OPTIONAL_STRING,
    location: _.assign({}, SchemaTypes.POINT_COORDINATES, { required: false, }),
    address: SchemaTypes.OPTIONAL_STRING,
    service_name: SchemaTypes.OPTIONAL_STRING,
};

const ACTOR_SCHEMA = {
    id: SchemaTypes.MANDATORY_OBJECT_ID,
    name: SchemaTypes.OPTIONAL_STRING,
    avatar: SchemaTypes.OPTIONAL_STRING,
    email: SchemaTypes.OPTIONAL_STRING,
    mobile: SchemaTypes.OPTIONAL_STRING,
};

const GARAGE_DETAIL_SCHEMA = {
    id: SchemaTypes.MANDATORY_OBJECT_ID,
    name: SchemaTypes.MANDATORY_STRING,
    avatar: SchemaTypes.OPTIONAL_STRING,
    address: SchemaTypes.OPTIONAL_STRING,
    location: SchemaTypes.POINT_COORDINATES,
    rating: SchemaTypes.OPTIONAL_STRING,
};

const QUOTE_DETAIL_SCHEMA = {
    estimated_price: SchemaTypes.OPTIONAL_STRING,
    quote_tags: { type: [SchemaTypes.OPTIONAL_STRING], required: false, },
    fare_breakup: [{
        key: SchemaTypes.OPTIONAL_STRING,
        value: SchemaTypes.OPTIONAL_NUMBER,
    }],
};

/**
 Model to store Quotation Request
 */
const QuotationSchema = new GTSchema({
    request_id: _.assign({}, SchemaTypes.MANDATORY_STRING, { default: shortId.generate, }),
    requested_by: ACTOR_SCHEMA,
    status: _.assign({}, SchemaTypes.MANDATORY_STRING, { enum: quotationStatus.values, default: quotationStatus.CREATED, }),
    garage_id: SchemaTypes.MANDATORY_OBJECT_ID,
    is_reviewed: SchemaTypes.MANDATORY_BOOLEAN,
    request_detail: REQUEST_DETAIL_SCHEMA,
    garage_detail: GARAGE_DETAIL_SCHEMA,
    quote_detail: QUOTE_DETAIL_SCHEMA,
    is_user_accepted_quote: SchemaTypes.MANDATORY_BOOLEAN,
    communication: {
        request_sent_to_garage_via_mail: SchemaTypes.MANDATORY_BOOLEAN,
        quote_sent_to_user_via_mail: SchemaTypes.MANDATORY_BOOLEAN,
    },
});

QuotationSchema.index({ request_id: 1, });
QuotationSchema.index({ request_id: 1, created_at: -1, });
QuotationSchema.index({ request_id: 1, status: 1, });
QuotationSchema.index({ 'requested_by.id': 1, 'status': 1, });

module.exports = conn.model('quotations', QuotationSchema);
