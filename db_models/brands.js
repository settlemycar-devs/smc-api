const conn = require('../modules/db');
const GTSchema = require('./gt-schema');

const SchemaTypes = require('../utils').schemaTypes();

const Brands = new GTSchema({
    brand: SchemaTypes.MANDATORY_STRING,
    brand_models: [SchemaTypes.MANDATORY_STRING],
});

module.exports = conn.model('brands', Brands);
