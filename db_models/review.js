const conn = require('../modules/db');

const GTSchema = require('./gt-schema');
const _ = require('lodash');
const { mediaTypes: MediaTypeConstants, } = require('../constants/schema');

const SchemaTypes = require('../utils').schemaTypes();

const reviewSchema = new GTSchema({
    rating: SchemaTypes.MANDATORY_NUMBER,
    title: SchemaTypes.MANDATORY_STRING,
    description: SchemaTypes.OPTIONAL_STRING,
    media: _.assign({ enum: MediaTypeConstants.values, }, SchemaTypes.OPTIONAL_STRING),
    garage_id: SchemaTypes.MANDATORY_OBJECT_ID,
    reviewed_by: {
        user_id: SchemaTypes.MANDATORY_OBJECT_ID,
        name: SchemaTypes.MANDATORY_STRING,
        avatar: SchemaTypes.OPTIONAL_STRING,
    },
});

// TODO:Implment the last three garage serive rating logic here.

module.exports = conn.model('reviews', reviewSchema);
