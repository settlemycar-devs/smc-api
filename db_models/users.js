const conn = require('../modules/db');
const bcrypt = require('bcryptjs');

const GTSchema = require('./gt-schema');
const Utils = require('../utils');
const SchemaTypes = require('../utils').schemaTypes();

const userSchema = new GTSchema({
    method: {
        type: String,
        enum: ['google', 'facebook', 'otp', 'local'],
        required: true,
    },
    email: {
        type: String,
        validate: [Utils.validateEmail, 'Invalid email'],
        unique: true,
    },
    phone: {
        type: String,
        unique: true,
    },
    password: SchemaTypes.OPTIONAL_STRING,
    google: {},
    facebook: {},
    is_email_verified: SchemaTypes.MANDATORY_BOOLEAN,
    profile: {
        name: {
            type: String,
        },
        avatar: {
            type: String,
        },
    },
    otp: SchemaTypes.OPTIONAL_STRING,
    roles: [{
        type: String,
        enum: ['admin', 'garage'],
        required: false,
    }],
});

userSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        // encrypt the password
        const salt = bcrypt.genSaltSync(10);
        const encryptedPassword = bcrypt.hashSync(this.password, salt);
        this.password = encryptedPassword;
    }
    next();
});

module.exports = conn.model('users', userSchema);
