const conn = require('../modules/db');
const _ = require('lodash');
const Utils = require('../utils');

const { MechanicSchema, } = require('./mechanics');

const SchemaTypes = Utils.schemaTypes();
const GTSchema = require('./gt-schema');

const FacilityItemSchema = {
    title: SchemaTypes.MANDATORY_STRING,
    icon: SchemaTypes.OPTIONAL_STRING,
    icon_name: SchemaTypes.OPTIONAL_STRING,
    is_available: SchemaTypes.MANDATORY_BOOLEAN,
};

const GarageInNumberItemSchema = {
    title: SchemaTypes.MANDATORY_STRING,
    total: SchemaTypes.MANDATORY_NUMBER,
    icon: SchemaTypes.OPTIONAL_STRING,
    icon_name: SchemaTypes.OPTIONAL_STRING,
};

const FaqItemSchema = {
    _id: _.assign({ auto: true, }, SchemaTypes.MANDATORY_OBJECT_ID),
    question: SchemaTypes.MANDATORY_STRING,
    answer: SchemaTypes.MANDATORY_STRING,
};

const GarageSchema = new GTSchema({
    name: SchemaTypes.MANDATORY_STRING,
    avatar: SchemaTypes.OPTIONAL_STRING,
    description: SchemaTypes.MANDATORY_STRING,
    category: SchemaTypes.OPTIONAL_STRING, // not using
    no_of_services: _.defaults({ default: 0, }, SchemaTypes.MANDATORY_NUMBER),
    address: SchemaTypes.MANDATORY_STRING,
    pincode: SchemaTypes.OPTIONAL_STRING,
    location: SchemaTypes.POINT_COORDINATES,
    about_us: {}, // p1, p2, p3, not using
    opening_time: {}, // MON: '9AM-3PM' not using
    portfolio: [SchemaTypes.MANDATORY_STRING],
    media: {
        source: SchemaTypes.OPTIONAL_STRING,
        thumbnail: SchemaTypes.OPTIONAL_STRING,
    },
    mechanics: [{ type: MechanicSchema, required: false, }],
    established_since: SchemaTypes.OPTIONAL_NUMBER, // year
    last_three_services: [], // FIXME:correct service type
    overall_rating: _.assign({ default: 0, }, SchemaTypes.MANDATORY_NUMBER),
    reviews: [{}], // NOT USING
    facilities: [FacilityItemSchema],
    garage_in_numbers: [GarageInNumberItemSchema],
    faqs: [FaqItemSchema],
    tags: [SchemaTypes.OPTIONAL_STRING],
    average_response_time: _.assign({ default: 0, }, SchemaTypes.MANDATORY_NUMBER), // in hours
    owner_info: { type: {
        name: SchemaTypes.OPTIONAL_STRING,
        mobile: SchemaTypes.MANDATORY_STRING,
        email: SchemaTypes.MANDATORY_STRING,
    }, required: false, },
});

GarageSchema.index({ name: 1, });
GarageSchema.index({ location: '2dsphere', });

const GarageModel = conn.model('garages', GarageSchema);

module.exports = GarageModel;
