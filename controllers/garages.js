const garageService = require('../services/garages');

const StandardResponse = require('../utils').getStandardResponse;
const _ = require('lodash');

class Garages {
    extractStandardOptions(req) {
        const options = {};
        const query = req.query;
        if (query.sort) {
            options.sort = query.sort;
            delete query.sort;
        }
        if (query.limit) {
            options.limit = Number(query.limit);
            delete query.limit;
        }
        if (query.skip) {
            options.skip = Number(query.skip);
            delete query.skip;
        }
        if (query.offset) {
            options.skip = Number(query.offset);
            delete query.offset;
        }
        return options;
    }

    async handleAddGarages(req, res, next) {
        try {
            const resData = await garageService.addGarage(req.user, req.body);
            return res.send(StandardResponse(resData));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleAddManyGarages(req, res, next) {
        try {
            const resData = await garageService.addManyGarages(req.body);
            return res.send(StandardResponse(resData));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleAddMechanic(req, res, next) {
        try {
            const id = req.params.id;
            const resData = await garageService.addMechanics(id, req.body);
            return res.send(StandardResponse(resData));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleGetById(req, res, next) {
        try {
            const id = req.params.id;
            const resData = await garageService.getGarageByQuery(req.user, { _id: id, });
            return res.send(StandardResponse(resData));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleGetAllGarages(req, res, next) {
        try {
            const { sort, limit, skip, } = this.extractStandardOptions(req);
            const resData = await garageService.getAllGarages(req.user, _.defaults({}, req.query, req.params), { sort, limit, skip, });
            return res.send(StandardResponse(resData.data, resData.meta));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleUploadProfile(req, res, next) {
        try {
            const updatedGarage = await garageService.updateGarage(req.user, req.params.id, { avatar: req.file.location, });
            res.send(StandardResponse({ avatar: updatedGarage.avatar, }));
        } catch (error) {
            res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleGallaryUpload(req, res, next) {
        try {
            const files = _.map(req.files, 'location');
            const updates = { portfolio: files, };
            const updatedGarage = await garageService.updateGarage(req.user, req.params.id, updates);
            res.send(StandardResponse({ portfolio: updatedGarage.portfolio, }));
        } catch (error) {
            res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleUpdateGarage(req, res, next) {
        try {
            const updatedGarage = await garageService.updateGeneric(req.user, req.params.id, req.body);
            res.send(StandardResponse(updatedGarage));
        } catch (error) {
            res.status(400).send({ error: true, message: error.message, });
        }
    }
}

module.exports = new Garages();
