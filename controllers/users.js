const _ = require('lodash');
const Utils = require('../utils');
const userService = require('../services/users');

class User {
    async handleGetProfile(req, res, next) {
        const profileData = await userService.getProfileFromUser(req.user);
        res.send(Utils.getStandardResponse(profileData));
    }

    async handleUpdateProfile(req, res, next) {
        const updatedProfile = await userService.updateUserProfile(req.user._id, _.assign(req.body, req.params));
        res.send(Utils.getStandardResponse(updatedProfile));
    }
}

module.exports = new User();
