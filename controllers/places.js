const PlaceService = require('../services/places');

class Places {
    handleAutocomplete(req, res, next) {
        const query = req.query;
        let ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        if (ipAddress.substr(0, 7) === '::ffff:') {
            ipAddress = ipAddress.substr(7);
        }
        // const ipAddress = req.connection.remoteAddress;
        PlaceService.getAutocompletePlaces(query, ipAddress).then(result => {
            res.send({ data: result, });
        }).catch(() => res.status(400).send({}));
    }

    handleDetails(req, res, next) {
        const query = req.query;
        PlaceService.getPlaceDetails(query).then(result => {
            res.send({ data: result, });
        }).catch(() => res.status(400).send({}));
    }
}

module.exports = new Places();
