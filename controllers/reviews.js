const ReviewService = require('../services/reviews');
const StandardResponse = require('../utils').getStandardResponse;
const _ = require('lodash');

class Reviews {
    async handleAdd(req, res, next) {
        try {
            const resData = await ReviewService.addReview(_.assign(req.body));
            return res.send(StandardResponse(resData));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleGetAll(req, res, next) {
        try {
            const resData = await ReviewService.getAllReviews(_.assign(req.query, req.params));
            return res.send(resData);
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }
}

module.exports = new Reviews();
