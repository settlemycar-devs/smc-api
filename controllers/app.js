/* eslint-disable class-methods-use-this */
const appService = require('../services/app');
const Utils = require('../utils');

class App {
    async handleInit(req, res, next) {
        try {
            const resData = await appService.getBrands(req.user);
            return res.send(Utils.getStandardResponse(resData));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }

    async handleAddBrands(req, res, next) {
        try {
            const resData = await appService.handleBulkInsert(req.user, req.body);
            return res.send(Utils.getStandardResponse(resData));
        } catch (error) {
            return res.status(400).send({ error: true, message: error.message, });
        }
    }
}

module.exports = new App();
