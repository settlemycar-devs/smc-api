const quotationService = require('../services/quotation');
const Utils = require('../utils');

const StandardResponse = Utils.getStandardResponse;
const StandardErrorResponse = Utils.getStandardErrorResponse;
const _ = require('lodash');

class Quotation {
    extractStandardOptions(req) {
        const options = {};
        const query = req.query;
        if (query.sort) {
            try {
                const sort = JSON.parse(query.sort);
                options.sort = sort;
                delete query.sort;
            } catch (error) {
                console.log('error in sort extraction', error);
            }
        }
        if (query.limit) {
            options.limit = Number(query.limit);
            delete query.limit;
        }
        if (query.skip) {
            options.skip = Number(query.skip);
            delete query.skip;
        }
        if (query.offset) {
            options.skip = Number(query.offset);
            delete query.offset;
        }
        if (query.filter) {
            // TODO: Add more validation here
            try {
                const filter = JSON.parse(query.filter);
                options.filter = filter;
                delete query.filter;
            } catch (error) {
                console.log('error in filter extraction', error);
            }
        }
        return options;
    }

    async handleReviewQuote(req, res, next) {
        try {
            const response = await quotationService.reviewQuote(req.user, _.assign({}, req.query, req.params));
            res.send(StandardResponse(response));
        } catch (error) {
            res.status(400).send(StandardErrorResponse(error.message));
        }
    }

    async handleCreateQuoteRequest(req, res, next) {
        try {
            const response = await quotationService.addQuoteRequest(req.user, req.body);
            res.send(StandardResponse(response));
        } catch (error) {
            res.status(400).send(StandardErrorResponse(error.message));
        }
    }

    async handleGetAllRequetedQuote(req, res, next) {
        try {
            const response = await quotationService.getAllQuoteRequestByUser(req.user._id);
            res.send(StandardResponse(response.data, response.meta));
        } catch (error) {
            res.status(400).send(StandardErrorResponse(error.message));
        }
    }

    async handleUpdateRequest(req, res, next) {
        try {
            const quoteRequestId = _.get(req.params, 'id');
            const response = await quotationService.updateQuoteRequest(req.user, quoteRequestId, _.defaults(req.body));
            res.send(StandardResponse(response));
        } catch (error) {
            res.status(400).send(StandardErrorResponse(error.message));
        }
    }

    async handleAcceptRequest(req, res, next) {
        try {
            const quoteRequestId = _.get(req.params, 'id');
            const response = await quotationService.acceptQuoteRequest(req.user, quoteRequestId, _.defaults(req.body));
            res.send(StandardResponse(response.data, response.meta));
        } catch (error) {
            res.status(400).send(StandardErrorResponse(error.message));
        }
    }

    async handleGetAllQuoteRequest(req, res, next) {
        try {
            const { sort, limit, skip, filter, } = this.extractStandardOptions(req);
            const response = await quotationService.getAllQuoteRequest(req.user, _.assign({ filter, }, req.params, req.query), { sort, limit, skip, });
            res.send(response);
        } catch (error) {
            res.status(400).send(StandardErrorResponse(error.message));
        }
    }
}

module.exports = new Quotation();
