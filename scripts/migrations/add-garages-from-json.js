/*
run command:
node scripts/migrations/add-garages-from-json.js --file_path=/path/to/file.json
*/

const argv = require('yargs').argv;
const fs = require('fs');
const conn = require('../../modules/db');
const GarageService = require('../../services/garages');
const Promise = require('bluebird');

class AddBrands {
    init() {
        return new Promise((resolve, reject) => {
            this.filePath = argv.file_path;
            if (!this.filePath) {
                reject(Error('FilePath is required'));
            }
            resolve();
        });
    }

    verifyGarageData(data) {
        return [true, data];
    }

    processData() {
        const successList = [];
        const skipList = [];
        const rawdata = fs.readFileSync(this.filePath);
        const garageData = JSON.parse(rawdata);
        return Promise.each(garageData, garage => {
            const [isValid, validatedGarageData] = this.verifyGarageData(garage);
            if (!isValid) {
                return skipList.push(garage);
            }
            return GarageService.addGarage(null, validatedGarageData).then(result => {
                successList.push(result);
            });
        }).then(result => {
            console.log('done adding garages', result.length);
        });
    }

    closeConnection() {
        return new Promise((resolve, reject) => {
            conn.close();
            setTimeout(() => {
                resolve();
            }, 5000);
        });
    }

    start() {
        this.init()
            .then(() => this.processData())
            .then(() => this.closeConnection())
            .catch(error => {
                console.log(error);
            });
    }
}

const obj = new AddBrands();

obj.start();
