const argv = require('yargs').argv;
const csv = require('csv-parser');
const fs = require('fs');
const conn = require('../../modules/db');
const appService = require('../../services/app');

class AddBrands {
    init() {
        return new Promise((resolve, reject) => {
            this.filePath = argv.file_path;
            if (!this.filePath) {
                reject(Error('FilePath is required'));
            }
            resolve();
        });
    }

    processData() {
        const results = [];
        return new Promise((resolve, reject) => {
            fs.createReadStream(this.filePath)
                .pipe(csv())
                .on('data', row => {
                    results.push(row);
                })
                .on('end', async () => {
                    const uniqueBrands = new Map();
                    results.forEach(result => {
                        if (!uniqueBrands.get(result.car_brand)) {
                            uniqueBrands.set(result.car_brand, [result.car_model]);
                            return;
                        }
                        if (!uniqueBrands.get(result.car_brand).includes(result.car_model)) {
                            uniqueBrands.get(result.car_brand).push(result.car_model);
                        }
                    });
                    const data = [];
                    for (const [brand, brandModels] of uniqueBrands.entries()) {
                        data.push({
                            brand,
                            brand_models: brandModels,
                        });
                    }
                    try {
                        const result = await appService.handleBulkInsert({}, data);
                        console.log('result', result);
                        resolve();
                    } catch (error) {
                        throw error;
                    }
                });
        });
    }

    closeConnection() {
        return new Promise((resolve, reject) => {
            conn.close();
            setTimeout(() => {
                resolve();
            }, 5000);
        });
    }

    start() {
        this.init()
            .then(() => this.processData())
            .then(() => this.closeConnection())
            .catch(error => {
                console.log(error);
            });
    }
}

const obj = new AddBrands();

obj.start();
