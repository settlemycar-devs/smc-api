const _ = require('lodash');
const argv = require('yargs').argv;
const fs = require('fs');
const sesService = require('../../services/aws/ses');
const introTemplate = require('./intro-mail-template').intro;

class IntroMailCampaign {
    init() {
        return new Promise((resolve, reject) => {
            this.filePath = argv.file_path;
            if (!this.filePath) {
                reject(Error('FilePath is required'));
            }
            resolve();
        });
    }

    getEmailBody(emailData) {
        return introTemplate(emailData);
    }

    processData() {
        return new Promise((resolve, reject) => {
            const emailDataBuffer = fs.readFileSync(this.filePath);
            const emailData = JSON.parse(emailDataBuffer.toString());
            const emailList = _.map(emailData.usersdata, 'email');
            const subject = 'A message from founder';
            const body = this.getEmailBody(emailData);
            sesService.sendMail({ sender: 'founder@garagetroop.com', bcc: emailList, subject, body, })
                .then(response => {
                    console.log('response', response);
                    resolve();
                }).catch(error => {
                    console.log('error:', error);
                    reject(error);
                });
        });
    }

    closeConnection() {
        return new Promise((resolve, reject) => {
            resolve();
        });
    }

    start() {
        this.init()
            .then(() => this.processData())
            .then(() => this.closeConnection())
            .catch(error => {
                console.log(error);
            });
    }
}

const obj = new IntroMailCampaign();

obj.start();
