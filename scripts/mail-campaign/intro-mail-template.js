
function intro(emailData) {
    return `
<p>
<strong>Hello everyone,</strong>
</p>
<p>
I am going to launch a product which will solve your problem which you face often but lost all the hope to get it solved. So I request you to please stay and go through the complete email because atlast I have a special message for you.
</p>
<p>
<strong>What is it?</strong> 
</p>
<p>
---It is an online garage discovery platform for car owners.
</p>
<p>
It's going to be called the 
</p>
<p>
"<strong><code>Garage Troop</code></strong>(https://garagetroop.com/)"




<pre class="prettyprint">How will it benefit you?
</pre>


<ol>

<li>Quick garage profile, visiting like experience directly through mobile/laptop <span style="text-decoration:underline;">just like you look for hotels on <strong><code>OYO</code></strong>.</span>

<li>Without even calling anyone, get estimates from requested garages for repair/service directly on your mail <span style="text-decoration:underline;">just like you get quotes from <strong><code>Indiamart</code></strong>.</span>

<li>Compare garage quality, price, pickup availability, home service, Estimated time taken etc. <span style="text-decoration:underline;">Just like you check everything on<strong><code> Amazon</code></strong></span>.Save more than you ever thought.

<li>24/7 support desk to get answers for all types of queries <span style="text-decoration:underline;">just like you get support on <strong><code>Amazon </code></strong>anytime.</span>

<li>Access to a premium group of car owners to get tips on optimising performance of your car directly on your mobile phone.
</li>
</ol>



<pre class="prettyprint">Garagetroop is for serious car owners who are really frustrated with:
</pre>


<ul>

<li>Their experience with garages, 

<li>Dialling to garages for estimates,

<li>Unprofessional attitude of garage towards deadline of delivery of car. 

<li>Unable to find right workshops which suits their car or

<li>Many times they feel cheated at workshops.
</li>
</ul>
<p>
I have worked in this sector for more than 5 years, I have witnessed customer's frustration. And then I decided to bring a comparison platform for car owners which will help them to make better decisions and can get their service with peace of mind.
</p>



<pre class="prettyprint">Now Message Part:
</pre>


<p>
I have developed this platform for you. There is no business mindset behind it. Even I have to bear some losses to set up these things for you. I can expect one thing from you in return, if you don’t mind! And that is, <strong><code>Please reply "YES" to this email</code></strong> if you think that Garagetroop can help you to remove your frustrations when you need car repair/services.

<p>
After you reply “YES”, I will let you know how to get started with Garagetroop. Waiting for your turn.
</p>
<p>
Thanks
</p>
<p>
Garagetroop ( <strong><code>Platform truly for car owners </code></strong>)




<pre class="prettyprint">For any assistance:
</pre>


<p>
Call: +91 9871564092
</p>
<p>
Email: help@garagetroop.com
</p>
`;
}

module.exports = {
    intro,
};
