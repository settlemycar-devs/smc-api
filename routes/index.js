const authMiddleware = require('../middlewares/auth');
/* eslint-disable global-require */
function initiateRoutes(app) {
    app.get('/ping', (req, res) => {
        res.send('pong');
    });
    app.use('/app', authMiddleware.addRequestedUser, require('./app'));
    app.use('/auth', authMiddleware.addRequestedUser, require('./auth'));
    app.use('/garages', authMiddleware.addRequestedUser, require('./garages'));
    app.use('/places', authMiddleware.addRequestedUser, require('./places'));
    app.use('/reviews', authMiddleware.addRequestedUser, require('./reviews'));
    app.use('/user', authMiddleware.addRequestedUser, require('./profile'));
    app.use('/quotation', authMiddleware.addRequestedUser, require('./quotation'));
}

module.exports = initiateRoutes;
