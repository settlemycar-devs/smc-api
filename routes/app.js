const router = require('express').Router();
const appController = require('../controllers/app');

router.get('/init', appController.handleInit.bind(appController));
router.post('/add-brands', appController.handleAddBrands.bind(appController));

module.exports = router;
