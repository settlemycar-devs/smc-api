const router = require('express').Router();
const ReviewsController = require('../controllers/reviews');

router.post('/', ReviewsController.handleAdd.bind(ReviewsController));
router.get('/', ReviewsController.handleGetAll.bind(ReviewsController));

module.exports = router;
