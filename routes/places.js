const router = require('express').Router();
const PlacesController = require('../controllers/places');

router.get('/autocomplete', PlacesController.handleAutocomplete);
router.get('/details', PlacesController.handleDetails);

module.exports = router;

