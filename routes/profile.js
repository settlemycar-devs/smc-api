const router = require('express').Router();
const userController = require('../controllers/users');
const authMiddleware = require('../middlewares/auth');

router.get('/profile', authMiddleware.isLoggedIn, userController.handleGetProfile.bind(userController));
router.post('/profile', authMiddleware.isLoggedIn, userController.handleUpdateProfile.bind(userController));

module.exports = router;
