const router = require('express').Router();
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const uuid = require('uuid').v4;
const GarageController = require('../controllers/garages');
const authMiddleware = require('../middlewares/auth');
const { garages: garageValidation, } = require('../middlewares/validation');
const config = require('config');

const accesKey = config.get('aws.AWS_ACCESS_KEY');
const secretKey = config.get('aws.AWS_SECRET_KEY');
const region = config.get('aws.AWS_SMS_REGION');
const bucket = config.get('aws.AWS_BUCKET');
const profileFolder = config.get('aws.GARAGE_PROFILE_FOLDER');
const galleryFolder = config.get('aws.GARAGE_GALLARY_FOLDER');

AWS.config.update({
    accessKeyId: accesKey,
    secretAccessKey: secretKey,
    region,
});

const s3 = new AWS.S3();
const uploadAvatar = multer({
    storage: multerS3({
        s3,
        bucket,
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: (req, file, cb) => {
            const garageId = req.params.id;
            const key = `${profileFolder}/${garageId}`;
            cb(null, key);
        },
    }),
});

const uploadGallary = multer({
    storage: multerS3({
        s3,
        bucket,
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: (req, file, cb) => {
            const garageId = req.params.id;
            const uniqueId = uuid();
            const key = `${galleryFolder}/${garageId}_${uniqueId}`;
            cb(null, key);
        },
    }),
});

router.get('/', garageValidation.validateGetAll, GarageController.handleGetAllGarages.bind(GarageController));
router.get('/:id', GarageController.handleGetById.bind(GarageController));
router.post('/', authMiddleware.isLoggedIn, authMiddleware.isAdmin, GarageController.handleAddGarages.bind(GarageController));
router.post('/multi', authMiddleware.isLoggedIn, authMiddleware.isAdmin, GarageController.handleAddManyGarages.bind(GarageController));

router.post('/:id', authMiddleware.isLoggedIn, authMiddleware.isAdmin, GarageController.handleUpdateGarage.bind(GarageController));


router.post(
    '/:id/upload-avatar',
    authMiddleware.isLoggedIn,
    authMiddleware.isAdmin,
    uploadAvatar.single('avatar'),
    GarageController.handleUploadProfile.bind(GarageController)
);

router.post(
    '/:id/upload-gallery',
    authMiddleware.isLoggedIn,
    authMiddleware.isAdmin,
    uploadGallary.array('files', 10),
    GarageController.handleGallaryUpload.bind(GarageController)
);

// router.post('/:id/mechanic', authMiddleware.isValidSecretKey, GarageController.handleAddMechanic.bind(GarageController));
module.exports = router;
