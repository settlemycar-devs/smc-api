const router = require('express').Router();
const authMiddleware = require('../middlewares/auth');
const quotationController = require('../controllers/quotation');

router.get('/requested-quotes', authMiddleware.isLoggedIn, quotationController.handleGetAllRequetedQuote.bind(quotationController));
router.get('/review-request', authMiddleware.isLoggedIn, quotationController.handleReviewQuote.bind(quotationController));
router.post('/create-request', authMiddleware.isLoggedIn, quotationController.handleCreateQuoteRequest.bind(quotationController));
router.post('/:id/update-request', authMiddleware.isLoggedIn, quotationController.handleUpdateRequest.bind(quotationController));
router.post('/:id/accept-request', authMiddleware.isLoggedIn, quotationController.handleAcceptRequest.bind(quotationController));

router.get('/all-quotes', authMiddleware.isLoggedIn, authMiddleware.isAdmin, quotationController.handleGetAllQuoteRequest.bind(quotationController));

module.exports = router;
