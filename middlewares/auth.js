const config = require('config');
const _ = require('lodash');
const Utils = require('../utils');
const userService = require('../services/users');

class AuthMiddleware {
    isValidSecretKey(req, res, next) {
        const secretKey = req.headers.secret_key;
        if (config.get('app.SECRET_KEY') !== secretKey) {
            return res.status(403).send({ error: true, message: 'Not allowed to perform action', });
        }
        req.isValidSecret = true;
        return next();
    }

    async isLoggedIn(req, res, next) {
        if (req.user) {
            return next();
        }
        return res.status(401).send({ message: 'Invalid Token', });
    }

    async isAdmin(req, res, next) {
        if (req.user && req.user.roles && req.user.roles.includes('admin')) {
            return next();
        }
        return res.status(403).send({ message: 'You must be admin to perform the task', });
    }

    async addRequestedUser(req, res, next) {
        const authHeader = _.get(req.headers, 'authorization');
        if (!authHeader) {
            return next();
        }
        try {
            const token = authHeader.split(' ')[1];
            const decodedToken = Utils.verifyJwt(token);
            const userId = _.get(decodedToken, 'id');
            if (!userId) {
                return next();
            }
            const user = await userService.getOne({ _id: userId, });
            if (!user) {
                return next();
            }
            req.user = user;
            return next(null);
        } catch (error) {
            return next();
        }
    }
}

module.exports = new AuthMiddleware();
