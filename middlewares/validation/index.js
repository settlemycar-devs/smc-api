const garageValidation = require('./garages');

module.exports = {
    garages: garageValidation,
};
