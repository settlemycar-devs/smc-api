// eslint-disable-next-line id-length
const J = require('@hapi/joi');
const _assign = require('lodash/assign');

class GarageValidation {
    validateGetAll(req, res, next) {
        const schema = J.object({
            latitude: J.number,
            longitude: J.number,
        });
        const { value, errors, } = schema.validate(_assign(req.body, req.query, req.params));
        if (errors) {
            return res.status(400).send({ 'error': { detail: 'Invalid request', }, });
        }
        req.validated_data = value;
        next();
    }

    validateCreateRequestQuote(req, res, next) {
        const schema = J.object({
            garage_id: J.string,
        });
        const { value, errors, } = schema.validate(_assign(req.body, req.query, req.params));
        if (errors) {
            return res.status(400).send({ 'error': { detail: 'Invalid request', }, });
        }
        req.validated_data = value;
        next();
    }
}

module.exports = new GarageValidation();
