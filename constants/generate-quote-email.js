const _ = require('lodash');

const renderQuoteTags = quoteRequest => {
    const tags = [];
    const quoteTags = _.get(quoteRequest, 'quote_detail.quote_tags', []);
    for (let i = 0; i < quoteTags.length; i++) {
        if (tags.length === 3) {
            tags.push(`<div style="color:#535353;width:49%;display:inline-block;margin-bottom:25px">
            <a href="https://garagetroop.com/user/profile?quote=true">View More</a>
         </div>`);
            break;
        }
        tags.push(`<div style="color:#535353;width:50%;display:inline-block;margin-bottom:25px">
        <div style="margin-left:9px;display:inline-block;font-size:16px;line-height:14px">${quoteTags[i]}</div>
     </div>`);
    }
    return tags.join('');
};

module.exports = quoteRequest => {
    return `
    <div style="font-family:Lato,Helvetica,Arial,sans-serif;font-size:16px;color:#282c3f;line-height:1.5">
    <div class="adM"></div>
    <center>
       <table style="width:680px!important" width="500">
          <tbody>
             <tr>
                <td>
                  <table border="0" style="width:100%;border:solid 1px #fff;padding:10px 30px 30px 30px">
                      <tbody>
                         <tr>
                            <td align="center" style="background:#1f396c;padding: 25px;">
                               <img src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/gt_logo_1.png" class="CToWUd a6T" tabindex="0">
                               <div class="a6S" dir="ltr" style="opacity: 0.01; left: 690.734px; top: 128px;">
                                  <div id=":29o" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download">
                                     <div class="aSK J-J5-Ji aYr"></div>
                                  </div>
                               </div>
                            </td>
                         </tr>
                         <tr style="width:100%">
                            <td align="left" style="padding: 20px 0 0 0;">
                               <b>Hi,</b>
                            </td>
                         </tr>
                         <tr style="width:100%">
                            <td align="left" style="padding: 20px 0;"> <b style="font-size:20px"> ${_.get(quoteRequest, 'garage_detail.name', '')} has provided an estimate for your ${_.get(quoteRequest, 'request_detail.brand_name', '')} ${_.get(quoteRequest, 'request_detail.model', '')}(${_.get(quoteRequest, 'request_detail.year', '')}) & ${_.get(quoteRequest, 'request_detail.service_name', '')} </b> </td>
                         </tr>
                         <tr>
                            <td>
                               <div style="height: 4px;
                                  background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(212,58,163,1) 58%, rgba(36,0,255,1) 98%);"></div>
                            </td>
                         </tr>
                         <tr>
                            <td style="padding:0px 0px -6px 0px;text-align:left">
                               <b style="font-size:25px">Quote Details</b><br>
                            </td>
                         </tr>
                         <tr>
                            <td style="padding:0px 0px 20px 0px;text-align:left">
                               <span style="color:#7e818b">Request ID.:</span> ${_.get(quoteRequest, 'request_id', '')}
                            </td>
                         </tr>
                      </tbody>
                  </table>

                  <table role="module" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                    <tbody><tr>
                      <td style="padding:0px 0px 0px 0px;background-color:#ffffff" height="100%" valign="top" bgcolor="#ffffff">
                          <div>
                              <div style="width: 86%;font-family:Arial;border-radius:5px;padding:10px;color:#000;margin:auto;margin-bottom:10px;border:1px solid #d5d5d5;">
                                 <div style="width:29%;display:inline-block">
                                    <img src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/garage_profile_mail.png" style="width:auto;max-height:75px" tabindex="0">
                                 </div>
                                 <div style="width:70%;display:inline-block">
                                    <div style="color:#cb202d;font-size:25px;font-weight:bold;margin-bottom:10px">${_.get(quoteRequest, 'garage_detail.name', '')}</div>
                                    <div style="font-size:15px">${_.get(quoteRequest, 'garage_detail.address', '')}</div>
                                    <div style="display:inline-block;margin-right:10px">
                                       <span style="font-size:26px;margin-right:15px;font-weight:bold">${_.get(quoteRequest, 'garage_detail.rating', '4.2')}</span>
                                       <img src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/star_yellow.png" style="background-size:100% 100%;h:28px;height:28px;display:inline-block;margin-right:3px" class="CToWUd">
                                    </div>
                                 </div>
                                 <div style="font-size: 20px;font-weight: bold;padding: 10px 0;padding-bottom: 0px"><span>Service:</span> ${_.get(quoteRequest, 'request_detail.service_name', '')} </div>
                                 <div style="font-weight: bold;font-size: 20;padding: 10px 0;">It Included</div>
                                 <div style="margin-top:15px">
                                    ${renderQuoteTags(quoteRequest)}
                                 </div>
                                 <div style="border-radius:4px;border:1px solid #dcdcdc;padding:8px;text-align:center;font-weight:bold;font-size:22px;background: #f0f8ff;">
                                    <div style="padding: 5px;">Total Estimate<sup>*</sup></div>
                                    <div>₹ ${_.get(quoteRequest, 'quote_detail.estimated_price')}</div>
                                 </div>
                              </div>
                          </div>
                      </td>
                    </tr>
                  </tbody>
                  </table>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="text-align:center;margin-top: 10px;">
                     <tbody>
                        <tr>
                           <td align="center" bgcolor="#1f396c" style="border-radius:30px;font-size:16px;text-align:center;background-color:inherit">
                              <a style="background-color:#1f396c;width:225px;font-size:16px;font-family:Helvetica,Arial,sans-serif;color:#ffffff;padding:18px;text-decoration:none;border-radius:30px;border:1px solid #1f396c;display:inline-block;border-color:#1f396c;font-weight:bold" href="https://garagetroop.com/user/profile?quote=true">
                                 View Full Estimate
                              </a>
                           </td>
                        </tr>
                     </tbody>
                  </table>

                  <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" style="padding:0px;background-color:#1f396c;box-sizing:border-box;margin-top: 50px;" bgcolor="#1f396c">
                     <tbody><tr role="module-content">
                       <td height="100%" valign="top">
                          <div style="padding: 15px;display: flex;">
                           <div style="width: 40%;display: inline-block;">
                              <img src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/gt_logo_1.png" style="width: 200px;"/>
                           </div>
                           <div style="width: 60%;display: inline-block;text-align: right;">
                              <a style="background-color: #f36262;border-radius: 5px;color: white;display: inline-block;outline: none;text-decoration: none;border: none;font-family: Helvetica,arial,sans-serif;" href="https://www.facebook.com/GarageTroop">
                                 <img style="width: 40px;height: 40px;" src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/facebook.png" />
                              </a>
                              <a style="margin-left:5px;background-color: #f36262;border-radius: 5px;color: white;display: inline-block;outline: none;text-decoration: none;border: none;font-family: Helvetica,arial,sans-serif;" href="https://www.instagram.com/garage_troop/">
                                 <img style="width: 40px;height: 40px;" src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/instagram.png" />
                              </a>
                              <a style="margin-left:5px;background-color: #f36262;border-radius: 5px;color: white;display: inline-block;outline: none;text-decoration: none;border: none;font-family: Helvetica,arial,sans-serif;" href="https://www.youtube.com/channel/UC1PjCJ1PaVEDTH5iz73lC7g">
                                 <img style="width: 40px;height: 40px;" src="https://garagetroop.s3-us-west-2.amazonaws.com/assets/youtube.png" />
                              </a>
                           </div>
                          </div>
                        <div style="padding: 15px;padding-top:0px;color: #fff;font-size: 20px;font-weight: 600;">
                           Email: business@garagetroop.com
                        </div>
                        <div style="padding: 15px; padding-top: 0px;color: #fff;font-size: 20px;font-weight: 600;">
                           Mobile: +91 6203 767 977
                        </div>
                        <div style="padding: 25px;color: #fff;font-size: 10px">
                           This Email was sent by garagetroop.com, to stop receiving email please mail to help@garagetroop.com with subject unsubscribe!
                        </div>
                        </td>
                     </tr>
          </tbody>
       </table>
    </center>
 </div>
    
    `;
};
