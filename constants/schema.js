const Enum = require('../classes/abstract_enum');

const mediaTypes = new Enum({
    VIDEO: 'video',
    IMAGE: 'image',
    AUDIO: 'audio',
});

const quotationStatus = new Enum({
    CREATED: 'created', // created
    REQUEST_SENT: 'request_sent', // request sent to garage
    QUOTE_SENT: 'quote_sent', // quote sent
    COMPLETED: 'completed', // completed request (means user has accepted the quote sent by garage)
});

module.exports = { mediaTypes, quotationStatus, };
