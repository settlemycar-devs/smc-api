const _ = require('lodash');
const generateQuoteEmails = require('./generate-quote-email');

const emailTemplates = {
    requestQuoteEmailBody() {
        return (`This message body contains HTML formatting. It can, for example, contain links like this one: <a class=\"ulink\" href=\"http://docs.aws.amazon.com/ses/latest/DeveloperGuide\" target=\"_blank\">Amazon SES Developer Guide</a>.`);
    },
    requestQuoteEmailSubject() {
        return (`New Quote Request for Maruti Suzuki`);
    },
    sentQuoteEmailBody(quoteRequest) {
        const body = generateQuoteEmails(quoteRequest);
        return body;
    },
    sentQuoteEmailSubject(quoteRequest) {
        return (`New Quote from ${_.get(quoteRequest, 'garage_detail.name', 'a Garage')}`);
    },
};


module.exports = {
    emailTemplates,
};
